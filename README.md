# README #

Comment utiliser le Konami Code et "hacker notre site" ?

C'est très simple (pas comme le vrai hacking) : il faut afficher la manette de jeu intégrée au site en cliquant sur le bouton "Gamepad" en bas de la page.
Une fois ceci fait, il faut entrer le Konami Code en utilisant les touches de la manette, et voilà ! Le site passe en mode "Hacké".
